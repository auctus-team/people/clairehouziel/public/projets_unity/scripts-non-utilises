using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Geometry;
using UnityEngine;

public class PoseEffectorSubscriber : MonoBehaviour
{
    //topic ROS �cout�
    [SerializeField]
    string topicName = "/panda/effector_coordinates";

    // Connecteur ROS 
    private ROSConnection rosConnection;

    // Singleton instance
    private static PoseEffectorSubscriber instance;

    // Get the singleton instance of this script
    public static PoseEffectorSubscriber Instance
    {
        get { return instance; }
    }

    // variables o� on stocke les derni�res valeurs publi�es 
    private Vector3 latestPosition;
    private Quaternion latestOrientation;


    // Start is called before the first frame update
    void Start()
    {
        // Initialisation de l'instance de ce script
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        // Connexion � ROS 
        rosConnection = ROSConnection.GetOrCreateInstance();
        rosConnection.Subscribe<PoseMsg>(topicName, ReceivePose);
    }

    private void ReceivePose(PoseMsg poseMsg)
    {
        // on actualise les valeurs de position et de rotation
        latestPosition = new Vector3((float)poseMsg.position.x, (float)poseMsg.position.y, (float)poseMsg.position.z);
        latestOrientation = new Quaternion((float)poseMsg.orientation.x, (float)poseMsg.orientation.y, (float)poseMsg.orientation.z, (float)poseMsg.orientation.w);

        // � d�commenter pour voir les donn�es sur la console Unity
        //Debug.Log("Latest Pose: Position(" + latestPosition + "), " +
        //          "Orientation(" + latestOrientation + ")");
    }

    //M�thodes publiques pour acc�der aux derni�res donn�es � partir d'un autre script
    public Vector3 LatestPosition
    {
        get { return latestPosition; }
    }

    public Quaternion LatestOrientation
    {
        get { return latestOrientation; }
    }


}
