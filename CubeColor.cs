using System.Collections;
using UnityEngine;

public class CubeColor : MonoBehaviour
{
    public GameObject robotEffector;
    public float distancemin = 0.35f;
    public Material targetMaterial;
    private Material originalMaterial;
    private Renderer cubeRenderer;
    [Range(0, 10)]
    public float speed = 1;

    private void Start()
    {
        cubeRenderer = GetComponent<Renderer>();
        originalMaterial = cubeRenderer.material;
    }

    private void Update()
    {
        // on calcule la distance entre l'effecteur du robot et le cube
        float distance = Vector3.Distance(transform.position, robotEffector.transform.position);

        if (distance < distancemin)
        {
            // quand l'effecteur se rapproche du cube, on le change de couleur 
            cubeRenderer.material = targetMaterial;
            //float lerp = Mathf.PingPong(Time.time * speed, 1);
            //cubeRenderer.material.Lerp(targetMaterial, invisibleMaterial, lerp);

        }

        else
        {
            // quand il s'en �loigne, on revient � la couleur d'origine
            cubeRenderer.material = originalMaterial;

        }
    }

}
