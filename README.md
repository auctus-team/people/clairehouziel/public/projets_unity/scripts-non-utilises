# Présentation

Il s'agit de scripts qui fonctionnent mais qui finalement n'ont pas été nécessaires pour le projet. 

Le script de PoseEffectorSubscriber fonctionne avec le node ROS effector_position présent dans le package [panda_unity](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity). Il récupère les coordonnées de l'effecteur par rapport à la base du robot. 

Le script TrajectoireDisplayROS récupère ces informations pour les appliquer au GameObject auquel il est associé. 

Finalement ils n'ont pas été nécessaires car Unity calcule lui-même la position et la rotation de l'effecteur par rapport à la base du robot. 

