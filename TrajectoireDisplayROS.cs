using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoireDisplayROS : MonoBehaviour
{
    public GameObject RobotBase;

    void Start()
    {
       
    }

    void Update()
    {
        // R�cup�rer la position du robot
        Vector3 targetPosition = RobotBase.transform.position;
        transform.position = targetPosition;

        // R�cup�rer l'orientation du robot
        Quaternion targetRotation = RobotBase.transform.rotation;
        transform.rotation = targetRotation;
    }
}
